const express = require('express');
const bodyParser = require('body-parser');

const app = express();

app.use(function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*'); // update to match the domain you will make the request from
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

let prevLogs = [
  {
    id: 1,
    date: new Date(2020, 11, 12),
    camera: 'Camera 1',
    event: 'Movement',
    image: 'https://bulma.io/images/placeholders/128x128.png',
  },
  {
    id: 2,
    date: new Date(2020, 10, 2),
    camera: 'Camera 3',
    event: 'Movement',
    image: 'https://bulma.io/images/placeholders/128x128.png',
  },
  {
    id: 3,
    date: new Date(2020, 4, 22),
    camera: 'Camera 1',
    event: 'Movement',
    image: 'https://bulma.io/images/placeholders/128x128.png',
  },
];

let newLogs = [];

function* idGenerator(n) {
  if (n > 3) {
    yield* idGenerator(n - 1);
    yield n;
  }
}

const getRandomInt = (min, max) => {
  return Math.floor(Math.random() * (max - min)) + min;
};

const nextId = idGenerator(100);

const generateData = () => {
  const id = nextId.next().value;
  newLogs.push({
    id,
    date: new Date(2020, getRandomInt(0, 12), getRandomInt(0, 31)),
    camera: `Camera ${id}`,
    event: 'Movement',
    image: 'https://bulma.io/images/placeholders/128x128.png',
  });
  // console.log(newLogs);
};

setInterval(generateData, 1000 * 20);

app.get('/logs', (req, res) => {
  res.json(newLogs);
  prevLogs = prevLogs.concat(newLogs);
  newLogs = [];
});

app.get('/history', (req, res) => {
  prevLogs = prevLogs.concat(newLogs);
  res.json(prevLogs);
  newLogs = [];
});

app.listen(9090, () => {
  console.log('Server listening on port 9090');
});
